<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewKeywordEvent' => [
            'App\Listeners\NewKeywordListener',
        ],
        'App\Events\NewVariantEvent' => [
            'App\Listeners\NewVariantListener',
        ],
        'App\Events\NewKeywordOccureceEvent' => [
            'App\Listeners\NewKeywordOccureceListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
