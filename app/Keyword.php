<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\NewKeywordEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Watson\Validating\ValidatingTrait;
use Watson\Validating\ValidationException;

class Keyword extends Model {

    protected $connection = 'mysql';
    protected $fillable = ['word', 'variant_id', 'party_id', 'category_id', 'favorite', 'province_id', 'channel'];
    protected $casts = [
        'favorite' => 'boolean',
    ];

    public function user() {
        return $this->belongsToMany(AppUser::class, 'subscriptions');
    }

    public function annotation() {
        $database = $this->getConnection()->getDatabaseName();
        return $this->belongsToMany(Annotation::class, "$database.annotation_keyword")->withPivot('thumbnail', 'id', 'count')->withTimestamps(); //->using(AnnotationKeyword::class);
    }

    public function province() {
        return $this->belongsTo(Province::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function party() {
        return $this->belongsTo(Party::class);
    }

    public function variant() {
        return $this->hasMany(Variant::class);
    }

    public static function exixtsKeyword($word) {
        if ($keyword = self::where("word", $word)->where('channel', env('APP_CHANNEL', 1))->first())
            return $keyword->id;
        return false;
    }

    /**
     * upload create thumbnail from video streaming
     * @param \App\Annotation $annotation
     */
    public function setThumbnail(Annotation $annotation, $time = NULL) {
        if (!isset($this->pivot->thumnail) || !$this->pivot->thumnail) {
            if ($data = get_m3u8_video_segment($annotation->getMedia('v'), $annotation->start_time)) {

                $time = $time ?: $data['start_time'];
                $video = $data['video'];
                $ffmpegFolder = storage_path('app/public/ffmpeg');
                $ffmpegFolderStream = $ffmpegFolder . '/' . $annotation->stream_id;
                $dest = $ffmpegFolderStream . '/' . $data['name'] . '.png';
                //only upload if the thumnail is not already uploaded and no localhost
                if (!file_exists($dest) && !isLocal()) {
                    if (!file_exists($ffmpegFolder)) {
                        mkdir($ffmpegFolder, 0777, true);
                        chown($ffmpegFolder, 'application');
                    }
                    if (!file_exists($ffmpegFolderStream)) {
                        mkdir($ffmpegFolderStream, 0777, true);
                        chown($ffmpegFolderStream, 'application');
                    }


                    $cmd = "ffmpeg -ss $time -i $video -an -vframes 1 -s 256X200 " . $dest;
                    var_dump($cmd);
                    exec($cmd);
                }
                $publicDest = 'ffmpeg/' . $annotation->stream_id . '/' . $data['name'] . '.png';
                if (!file_exists($dest) && $time > '00:00:04') {
                    $this->setThumbnail($annotation, '00:00:04');
                } else {
                    $annotation->keywords()->updateExistingPivot($this->id, ['thumbnail' => $publicDest]);
                }
            }
        }
    }

    public static function make($data) {
        $keyword = new self;
        //if not persons make sure not province or party
        if (in_array(array_get($data, 'category_id'), [1, 3])) {
            unset($data['party_id']);
            unset($data['province_id']);
        }
        $data['channel'] = env('APP_CHANNEL', 1);
        self::validate($data, $keyword);

        $keyword->fill(array_only($data, $keyword->fillable));
        $keyword->save();
        event(new NewKeywordEvent($keyword));
        return $keyword;
    }

    private static function validate($data, $model) {
        $rules = [
            'word' => ['required', 'string',
                Rule::unique('keywords')->where(function ($query) {
                            return $query->where('channel', env('APP_CHANNEL', 1));
                        })
            ],
            'province_id' => 'nullable|integer',
            'party_id' => 'nullable|integer',
            'category_id' => 'required|integer',
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator, $model);
        }
    }

}
