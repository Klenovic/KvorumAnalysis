<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Annotation;
use App\Keyword;
use Illuminate\Support\Facades\Storage;
use App\Services\Keywords\KeywordOccurences;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class monitorAddAnnotations extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:addannotations {words}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new occurences every new video/audio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {


        $words = $this->argument('words') < 50 ? 50 : $this->argument('words');
        $faker = Faker::create();
        $text = $faker->realText($words, 2);
        $cleanText = preg_replace('/[^a-zA-Z0-9 ]/', '', $text);
        var_dump($cleanText);
        $textArray = explode(' ', $cleanText);

        $sql = 'INSERT INTO `annotations` (`stream_id`, `start_time`, `end_time`, `word`, `letters_rate`,`created_at`,`updated_at`) VALUES';
        
        $coma = '';
        $now=time();
        $start = time()-$now;
        foreach ($textArray as $word) {
            $nowFormat = date('Y-m-d H:i:s');
            
            sleep(rand(0 * 100, 2 * 100) / 100);
            $end = $start + (rand(0 * 100, 1 * 100) / 100);

            $sql .= $coma . "(2, $start, $end, '$word', '0.3000','$nowFormat','$nowFormat')";
            $start = $end;
            $coma = ', ';
            $start=$end;
            
        }
        $sql .= ';';
        echo $sql;


        DB::connection('annotations_db_connection')->statement($sql);
        //check last stream_id








        exit;





        //query annotations order by id ASC from next stream_id if any
        //check against keywords for new ocurrences
    }

}
