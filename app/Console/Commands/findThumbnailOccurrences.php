<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class findThumbnailOccurrences extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:findThumbnails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'searches for thumnails occurrences which where not found when find occurrences was run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command. 
     *
     * @return mixed
     */
    public function handle() {
        try {
            \Log::debug('monitor:findThumbnails');

            $isLockedFile = 'isLockedThumbs.txt';
            $isLocked = 0;

            if (Storage::exists($isLockedFile))
                $isLocked = Storage::get($isLockedFile) ?: 0;
            else {
                Storage::put($isLockedFile, 0);
                $isLocked = 0;
            }

            if ($isLocked == 0) {
                Storage::put($isLockedFile, 1);


                $occurrences = \App\AnnotationKeyword::whereNull('thumbnail')->with('annotation')->with('keyword')->where('updated_at', '<', date('Y-m-d H:i:s', time() - (60 * 3)))->orderBy('id', 'ASC')->get();


                $occurrences->each(function($occurrence) {
                    $occurrence->keyword->setThumbnail($occurrence->annotation);
                });
                \Log::debug('monitor:findThumbnails processed ' . $occurrences->count());
                Storage::put($isLockedFile, 0);
            }
        } catch (\Exception $e) {
            \Log::debug('monitor:findThumbnails error exit ' . $e->getMessage() . ' file: ' . $e->getFile() . ' line: ' . $e->getLine());
            Storage::put($isLockedFile, 0);
        }
    }

}
