<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Annotation;
use App\Keyword;
use Illuminate\Support\Facades\Storage;
use App\Services\Keywords\KeywordOccurences;

class monitorAnnotations extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:annotations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new occurences every new video/audio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //check last stream_id

        try {

            $isLockedFile = 'isLocked.txt';
            if (Storage::exists($isLockedFile))
                $isLocked = Storage::get($isLockedFile) ?: 0;
            else {
                Storage::put($isLockedFile, 0);
                $isLocked = 0;
            }


            if ($isLocked == 0) {

                $lastIdFieldId = 'lastRecordId.txt';
                if (Storage::exists($lastIdFieldId))
                    $lastId = Storage::get($lastIdFieldId) ?: 0;
                else {
                    Storage::put($lastIdFieldId, 0);
                    $lastId = 0;
                }

                $newAnnotations = Annotation::where('id', '>', $lastId)->orderBy('id', 'ASC')->take(1050)->get();

                if ($newAnnotations->count()) {
                    \Log::debug('new cron process starts ' . print_r(['lastId' => $lastId, 'count' => $newAnnotations->count()], true));
                    Storage::put($isLockedFile, 1);
                    $keywords = Keyword::all()->each(function($keyWord)use($newAnnotations) {
                        $word = $keyWord->word;
                        (new KeywordOccurences())->findOccurences($newAnnotations, $word, $keyWord, false, false);
                        //process also variants
                        $keyWord->variant->each(function($variant)use($newAnnotations, $keyWord) {
                            (new KeywordOccurences())->findOccurences($newAnnotations, $variant->name, $keyWord, true, false);
                        });
                    });
                    Storage::put($lastIdFieldId, $newAnnotations->last()->id);
                    echo 'process completed, checked ' . $newAnnotations->count() . ' records';
                    Storage::put($isLockedFile, 0);
                    \Log::debug('cron process ends ' . print_r(['new lastId' => $newAnnotations->last()->id], true));
                } else {
                    echo 'No new records';
                }
            } else {
                echo 'Locked';
            }
        } catch (\Exception $e) {
            Storage::put($isLockedFile, 0);
            \Log::debug('new cron process error exit ' . $e->getMessage() . ' file: ' . $e->getFile() . ' line: ' . $e->getLine());
        }
    }

}
