<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Annotation;
use App\Keyword;
use Illuminate\Support\Facades\Storage;
use App\Services\Keywords\KeywordOccurences;

class monitorFindKeywordsByWord extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:findKeywordsByWords {words}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'find Occurrences for the specified words separated by coma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //check last stream_id


        var_dump($this->argument('words'));
        
        $newAnnotations=Annotation::whereIn('word',explode(',',$this->argument('words')))->orderBy('id','ASC')->get();
        
        //var_dump($newAnnotations->pluck('word'));exit;
        


        //var_dump(Keyword::where('id','>=',$this->argument('fromId'))->where('id','<=',$this->argument('toId'))->get());
        
        if ($newAnnotations->count()) {
            $keywords = Keyword::all()->each(function($keyword)use($newAnnotations) {
                $word = $keyword->word;
                $id = $keyword->id;
                (new KeywordOccurences())->findOccurences($newAnnotations, $word, $keyword);
                //var_dump($word);
            });
        }
        echo 'It is done, thanks!';

        


        exit;


        


        //query annotations order by id ASC from next stream_id if any
        //check against keywords for new ocurrences
    }

}
