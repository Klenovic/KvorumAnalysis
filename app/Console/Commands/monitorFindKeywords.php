<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Annotation;
use App\Keyword;
use Illuminate\Support\Facades\Storage;
use App\Services\Keywords\KeywordOccurences;

class monitorFindKeywords extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:findkeywords {fromId} {toId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new occurences every new video/audio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //check last stream_id


        var_dump($this->argument('fromId'));
        var_dump($this->argument('toId'));

        $newAnnotations = Annotation::all();
        
        //var_dump(Keyword::where('id','>=',$this->argument('fromId'))->where('id','<=',$this->argument('toId'))->get());
        
        if ($newAnnotations->count()) {
            $keywords = Keyword::where('id','>=',$this->argument('fromId'))->where('id','<=',$this->argument('toId'))->get()->each(function($keyword)use($newAnnotations) {
                $word = $keyword->word;
                $id = $keyword->id;
                (new KeywordOccurences())->findOccurences($newAnnotations, $word, $id);
                var_dump($word);
            });
        }
        echo 'It is done, thanks!';

        


        exit;


        


        //query annotations order by id ASC from next stream_id if any
        //check against keywords for new ocurrences
    }

}
