<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('monitor:annotations')->cron('* * * * *');
        $schedule->command('monitor:findThumbnails')->cron('*/3 * * * *');
        /* ->everyMinute();	Run the task every minute
          ->everyFiveMinutes();	Run the task every five minutes
          ->everyTenMinutes();	Run the task every ten minutes
          ->everyFifteenMinutes();	Run the task every fifteen minutes
          ->everyThirtyMinutes(); */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
