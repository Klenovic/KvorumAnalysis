<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Watson\Validating\ValidatingTrait;
use Watson\Validating\ValidationException;

class ForbiddenKeyword extends Model
{
    protected $rules = [
        'word' => 'required|string|unique:forbidden_keywords',
        
    ];
    protected $fillable = ['word'];

    public static function make($data){
        $keyword = new self;
        self::validate($data, $keyword);
        $keyword->fill(array_only($data, $keyword->fillable));
        $keyword->save();
        return $keyword;
    }
    
    private static function validate($data, $model) {
        $validator = Validator::make($data, $model->rules);
        if ($validator->fails()) {
            throw new ValidationException($validator, $model);
        }
    }
}
