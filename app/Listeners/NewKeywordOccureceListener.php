<?php

namespace App\Listeners;

use App\Events\NewKeywordOccureceEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Support\Facades\App;

class NewKeywordOccureceListener {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event to send notifications when new occurrences.
     *
     * @param  NewKeywordOccureceEvent  $event
     * @return void
     */
    public function handle(NewKeywordOccureceEvent $event) {

        \Log::debug('public function handle(NewKeywordOccureceEvent $event) { ' . $event->keyword->word . ' HasReal: ' . $event->keyword->realWord);
        $tokens = [];
        if (get_class($event->keyword) == 'App\Keyword') {
            if ($subscribedUsers = $event->keyword->user) {
                $subscribedUsers->each(function($user)use($event, &$tokens) {
                    //var_dump($user->device_id . ' Has: ' . $event->keyword->realWord);
                    \Log::debug($user->device_id . ' Has: ' . $event->keyword->word . ' HasReal: ' . $event->keyword->realWord);
                    if ($user->device_token)
                        $tokens[] = $user->device_token;
                });
                //\Log::debug('Tokens ' . print_r($tokens, true));
                $testing = '';
                if (isLocal() || isset($event->keyword->testing)) {// || isset($event->keyword->annotationModel)
                    $tokens = 
              ['fc66hnPP4nc:APA91bHQ0gW6MkNJsGnk8WIHu0m7lXp99GDBCy86C2ExmnVDVNdV3HjK2JonAr6_RCRGWlATcO3_FEJJIDtCt0m-P8LbTchN4GapvmJ5tIj1yz33VtqJKd3vHZ9DTGPUh2dtiOuIxVLP',
               '-6AqusmkbE:APA91bGxT_h-Zc3pkf6QUnsY_7r53Id5udU4vQNAF2dOX4Zc8W3NwhbwSJxxYEPSNMNCbtptXI30iotTorVdXWR3p_yrfgeNK5wRKKKZm1aH9MGXmnb8Sl9kLFGjFKb1A3wYlqLeNUVg',
               'evyw5bfO3Gw:APA91bEu9zOM4bVce_hzyLy69P4ISf56Nmm9jJgoaDqDYn_3zAblyFlHq4xn0QYb2a9zkuSeWTYr4qvm5uJ3NGGzrsxUWVWuPUbtY1vPNnPmQ3JUNxpfOCc2KPo1mSUvmnivBR6zLixY'];
                    $testing = 'testing- ';
                }
                if ($tokens) {
                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60 * 20);
                    $context = [];

                    $notificationBuilder = new PayloadNotificationBuilder($testing . "Najnovije: " . $event->keyword->word);
                    //var_dump($notificationBuilder->getIcon());exit;

                    \App\Annotation::where('id', '<', $event->keyword->annotationModel->id + 9)->where('id', '>', $event->keyword->annotationModel->id - 9)->get()->each(function($el)use(&$context) {
                        $context[] = $el->word;
                    });


                    $notificationBuilder->setBody('...' . implode(' ', $context) . '...')->setSound('default');

                    $dataBuilder = new PayloadDataBuilder();
                    $dataBuilder->addData([
                        'word' => $event->keyword->realWord,
                        'audio_link' => $event->keyword->annotationModel->getMedia('a'),
                        'video_link' => $event->keyword->annotationModel->getMedia('v'),
                        'audio_seek' => getStartTime($event->keyword->annotationModel->start_time),
                        'video_seek' => getStartTime($event->keyword->annotationModel->start_time),
                        'occurrence_id' => $event->keyword->pivot->id,
                        'stream_id' => $event->keyword->annotationModel->stream_id
                    ]);

                    $option = $optionBuilder->build();
                    $notification = $notificationBuilder->build();
                    $data = $dataBuilder->build();

                    $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
                    \Log::debug('$downstreamResponse ' . print_r($downstreamResponse, true));
                    \Log::debug('$data ' . print_r($data, true));

                    if(isset($event->keyword->annotationModel)){
                      print_r($downstreamResponse);
                      print_r($data);
                    }

//                    $downstreamResponse->numberSuccess();
//                    $downstreamResponse->numberFailure();
//                    $downstreamResponse->numberModification();
                }
            }
        }




        /*



          //return Array - you must remove all this tokens in your database
          $downstreamResponse->tokensToDelete();

          //return Array (key : oldToken, value : new token - you must change the token in your database )
          $downstreamResponse->tokensToModify();

          //return Array - you should try to resend the message to the tokens in the array
          $downstreamResponse->tokensToRetry();

          // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
          $downstreamResponse->tokensWithError(); */
    }

}
