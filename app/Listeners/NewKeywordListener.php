<?php

namespace App\Listeners;

use App\Events\NewKeywordEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Annotation;
use App\Services\Keywords\KeywordOccurences;

class NewKeywordListener implements ShouldQueue {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewKeywordEvent  $event
     * @return void
     */
    public function handle(NewKeywordEvent $event) {
        \Log::debug('$event ' . print_r(['$event' => $event,'object'=>get_class($event->keyword)], true));

        //var_dump($event);return;

        $isVariant = false;
        if (get_class($event->keyword) == 'App\Keyword') {
            $word = $event->keyword->word;
            $keyWord = $event->keyword;
        } elseif (get_class($event->keyword) == 'App\Variant') {
            $word = $event->keyword->name;
            $keyWord = $event->keyword->keyword;
            $isVariant = true;
        }
        $length = getKeywordLength($word);
        if ($length == 1) {
            $annotations = Annotation::whereWord($word)->get();
        } else {
            $words = explode(" ", $word);
            $words = array_map('cleanKeyword', $words);
            //$lasId=Annotation::orderBy('id','DESC')->first()->id;
            $annotations = Annotation::whereIn('word', $words)->get();
        }
        \Log::debug('new keyord/variant trigger find occurrences ' . print_r(['word' => $word, '$keyWord->id' => $keyWord->id], true));
        (new KeywordOccurences())->findOccurences($annotations, $word, $keyWord, $isVariant, true);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception) {
        \Log::debug('queue failed: ' . $exception->getMessage());
    }

}
