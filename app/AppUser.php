<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Watson\Validating\ValidatingTrait;
use Watson\Validating\ValidationException;

class AppUser extends Model {

    use ValidatingTrait;

    protected $rules = [
        'device_id' => 'required|string',
    ];
    protected $fillable = ['device_id', 'should_be_notified', 'device_token'];
    protected $cast = ['should_be_notified' => 'bool'];

    public function keywords() {
        return $this->belongsToMany(Keyword::class, 'subscriptions');
    }

    private function setKeyWords($keywords) {
        return $this->keywords()->attach($keywords);
    }

    public function getUserByDevice($device) {
        return $this->where('device_id', $device)->first();
    }

    public static function updateUserKeywords($data) {
        $user = new self;
        self::validate($data, $user);

        $appUser = $user->getUserByDevice($data['device_id']);
        if (!$appUser) {
            $appUser = $user->create(['device_id' => $data['device_id']]);
        }

        //set user device token to receive notifications
        if ($deviceToken = array_get($data, 'device_token')) {
            $appUser->device_token = $deviceToken;
            $appUser->save();
        }

        //add custom user keywords key
        $attachCustomKeywords = [];
        if ($keywordNames = array_get($data, 'keywords_name')) {

            foreach ((array) $keywordNames as $word) {
                if (!ForbiddenKeyword::where('word', $word)->first()) {
                    if ($id = Keyword::exixtsKeyword($word)) {
                        $attachCustomKeywords[] = $id;
                    } else {
                        $keyword = Keyword::make(['word' => $word, 'category_id' => '3']);
                        $attachCustomKeywords[] = $keyword->id;
                    }
                }
            }
        }

        //update user subscriptions
        $keywords = array_get($data, 'keywords', []);
        $keywordsToAttach = array_unique(array_merge($keywords, $attachCustomKeywords));
        $appUser->keywords()->detach();
        if ($keywordsToAttach)
            $appUser->setKeyWords($keywordsToAttach);



        return $appUser;
    }

    private static function validate($data, $model) {
        $validator = Validator::make($data, $model->rules);
        if ($validator->fails()) {
            throw new ValidationException($validator, $model);
        }
    }

}
