<?php

namespace App\Transformers;

use App\Keyword;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Facades\Cache;
use League\Fractal\ParamBag;

class VariantTransformer extends Transformer {

    /**
     * Transform the model data into a generic array.
     *
     * @param  Reservation $reservation
     * @return array
     */
    public function transform(Keyword $keyword): array {

        return [
            'keyword' => $keyword->word,
            'keywordId' => $keyword->id,
            'variants' => $keyword->variant,
        ];
    }

}
