<?php

namespace App\Transformers;

use App\Keyword;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Facades\Cache;
use League\Fractal\ParamBag;

class KeywordTransformer extends Transformer {

    /**
     * Transform the model data into a generic array.
     *
     * @param  Reservation $reservation
     * @return array
     */
    public function transform(Keyword $keyword): array {

        return [
            'id' => (int) $keyword->id,
            'user_count' => $keyword->user->count(),
            'favorite' => $keyword->favorite,
            'word' => $keyword->word,
            'channel' => $keyword->channel,
            'category'=>$keyword->category->name
            
        ];
    }

}
