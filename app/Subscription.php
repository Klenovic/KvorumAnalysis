<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {

    protected $rules = [
        'app_user_id' => 'required|integer',
        'keyword_id' => 'required|integer',
    ];
    protected $fillable = ['app_user_id', 'keyword_id'];

    public function user() {
        return $this->belongsTo(AppUser::class);
    }

    public function keyword() {
        return $this->belongsTo(Keyword::class);
    }

}
