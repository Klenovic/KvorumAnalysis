<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annotation extends Model {

    protected $connection = 'annotations_db_connection';

    public function stream() {
        return $this->belongsTo(Stream::class);
    }

    public function keywords() {
        return $this->belongsToMany(Keyword::class)->withPivot('thumbnail', 'id', 'count')->withTimestamps();
    }

    public function getMedia($type) {
        $type = $type == 'a' ? 'a' : 'v';
        return 'https://storage.googleapis.com/kvorum_test_bucket/app/video/' . $this->stream_id . '/' . $type . '/out.m3u8';
    }

}
