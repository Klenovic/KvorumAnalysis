<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;

class NewKeywordEvent {

    use Dispatchable,
        InteractsWithSockets,
        SerializesModels;

    public $keyword;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Model $keyword) {
        $this->keyword = $keyword;
    }

}
