<?php

namespace App\Http\Middleware;

use Closure;
use App\AppUser;
use Flugg\Responder\Facades\Responder;

class RegisteredMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $user = new AppUser;
        if (!$user->getUserByDevice($request->device_id)) {
            throw new \Exception('Forbidden.',403);
            
        }
        return $next($request);
    }

}
