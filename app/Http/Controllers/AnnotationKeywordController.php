<?php

namespace App\Http\Controllers;

use App\AnnotationKeyword;
use Illuminate\Http\Request;
use App\Events\NewKeywordOccureceEvent;
use Flugg\Responder\Facades\Responder;

class AnnotationKeywordController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $populated = AnnotationKeyword::orderBy('count', 'desc')->take(10)->get();
        $latest = AnnotationKeyword::orderBy('id', 'desc')->take(10)->get();
        return view('home', compact($populated, $latest));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function counter(AnnotationKeyword $keywordOcurrence) {
        try {
            $keywordOcurrence->count++;
            if ($keywordOcurrence->save())
                return Responder::success($keywordOcurrence);
            return Responder::error();
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

}
