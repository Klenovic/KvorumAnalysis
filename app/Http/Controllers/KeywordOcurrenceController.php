<?php

namespace App\Http\Controllers;

use App\KeywordOcurrence;
use App\AppUser;
use Illuminate\Http\Request;
use App\Events\NewKeywordOccureceEvent;
use App\Services\Keywords\KeywordGenerator;
use Flugg\Responder\Facades\Responder;

class KeywordOcurrenceController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        try {
            if ($request->has('device_id')) {
                $user = new AppUser;
                if ($appUser = $user->getUserByDevice($request->get('device_id'))) {
                    $keyWords = (new KeywordGenerator())->getWordsMainList($appUser);
                    $isRecording = $this->isRecording();

                    return Responder::success(['live_audio_url' => $isRecording, 'live_video_url' => $isRecording, 'listing' => $keyWords])->respond();
                } else {
                    $keyWords = (new KeywordGenerator())->getWordsByCategory();
                    $user->create(['device_id' => $request->get('device_id')]);
                    return Responder::success($keyWords)->respond();
                }
            } else {
                return Responder::error('Wrong query');
            }
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    function isRecording() {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://api.kvorum.hr/api/stream/is-recording');
        $status = $res->getStatusCode();
        $result = json_decode($res->getBody());

        if ($res->getStatusCode() == 200 && isset($result->isRecording) && $result->isRecording)
            return 'http://rec.tvwmedia.net/srcEncoders/tvwfeed1/playlist.m3u8';
        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function counter(KeywordOcurrence $keywordOcurrence) {
        dd($keywordOcurrence);
        $keywordOcurrence = KeywordOcurrence::find($occurrenceId);
        $keywordOcurrence->keyword_id = $request->get('keyword_id');
        $keywordOcurrence->annotation_id = $request->get('annotation_id');
        $keywordOcurrence->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $keywordOcurrence = new KeywordOcurrence();
        $keywordOcurrence->keyword_id = $request->get('keyword_id');
        $keywordOcurrence->annotation_id = $request->get('annotation_id');
        $keywordOcurrence->save();
        event(new NewKeywordOccureceEvent($keywordOcurrence));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KeywordOcurrence  $keywordOcurrence
     * @return \Illuminate\Http\Response
     */
    public function show(KeywordOcurrence $keywordOcurrence) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KeywordOcurrence  $keywordOcurrence
     * @return \Illuminate\Http\Response
     */
    public function edit(KeywordOcurrence $keywordOcurrence) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KeywordOcurrence  $keywordOcurrence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KeywordOcurrence $keywordOcurrence) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KeywordOcurrence  $keywordOcurrence
     * @return \Illuminate\Http\Response
     */
    public function destroy(KeywordOcurrence $keywordOcurrence) {
        //
    }

}
