<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Keyword;
use App\Services\Keywords\KeywordGenerator;
use Illuminate\Http\Request;
use Watson\Validating\ValidationException;
use Flugg\Responder\Facades\Responder;

class AppUserController extends Controller {

    /**
     * Return user's selected keywords
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        try {
            $keyWords = (new KeywordGenerator($request->get('device_id')))->getWordsByCategory();
            $labels=new \stdClass();
            $labels->themes='Committees';
            $labels->persons='People';
            $labels->custom='Custom';
            return Responder::success($keyWords)->meta(['label' => $labels])->respond();
        } catch (\Exception $e) {
            return Responder::error($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function show(AppUser $appUser) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function edit(AppUser $appUser) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            $appUser = AppUser::updateUserKeywords($request->all());
            return Responder::success();
        } catch (ValidationException $e) {
            return $this->validationErrorResponse($e);
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppUser $appUser) {
        //
    }

}
