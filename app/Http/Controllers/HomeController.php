<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppUser;
use App\Keyword;
use App\AnnotationKeyword;
use App\Annotation;
use App\Services\Keywords\KeywordGenerator;
use App\Http\Resources;
use Flugg\Responder\Facades\Responder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller {

    var $shareOccurrences = [];
    var $popularOccurrences = [];

    public function provisionalHome() {
        return $this->index('latest');
    }

    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type, $requestedVideo = null) {
        $data = [];
        $data['time'] = 0;
        $data['occurrences'] = [];
        $data['latest']=[];
        $data['url'] = '';
        $data['type'] = $type;
        $data['current_url'] = url()->full();
        $data['current_thumbnail'] = '';
        $data['sharePage'] = false;
        $data['homePage'] = true;
        $data['title'] = 'Kvorum';
        

        if ($requestedVideo) {
            $currentOccurrence = AnnotationKeyword::find($requestedVideo);
            if ($currentOccurrence) {
                $keyword = Keyword::find($currentOccurrence->keyword_id);
                $currentAnnotation = Annotation::find($currentOccurrence->annotation_id);
                $data['url'] = getBucketUrl($currentAnnotation->stream_id);
                $data['current_thumbnail'] = Config::get('app.url') . 'storage/' . $currentOccurrence->thumbnail;
                $data['time'] = getStartTime($currentAnnotation->start_time);
            }
        }
        $i = 0;


        //$popularOccurrences = AnnotationKeyword::orderBy('count', 'DESC')->with('annotation')->with('keyword')->take(20)->get();
        $popularOccurrences = \DB::table('annotation_keyword')
                ->join('keywords', 'annotation_keyword.keyword_id', '=', 'keywords.id')
                ->join('kvorum_prod.annotations', 'annotation_keyword.annotation_id', '=', 'kvorum_prod.annotations.id')
                ->select('annotation_keyword.thumbnail', 'annotation_keyword.id as ak_id', 'keywords.word', 'keywords.id as k_id', 
                        'kvorum_prod.annotations.id as a_id', 'kvorum_prod.annotations.created_at')
                //provisional filter by channel
                ->where('kvorum_prod.annotations.created_at','>','2019-01-25 00:00:00')->where('keywords.channel',env('APP_CHANNEL', 1))
                ->whereIn('keywords.category_id', [1, 2])
                ->orWhere(function($query) {
                    $query->where('keywords.favorite', 1)
                            //provisional filter by channel
                            ->where('kvorum_prod.annotations.created_at','>','2019-01-25 00:00:00')->where('keywords.channel',env('APP_CHANNEL', 1))
                            ->where('keywords.category_id', 3);
                })
                ->orderBy('annotation_keyword.count', 'DESC')
                ->take(20)
                ->get();
        if ($popularOccurrences->count()) {
            $data['occurrences'] = $this->setOccurrence($popularOccurrences, $requestedVideo, 'popular');
        }

        $latestOccurrences = \DB::table('annotation_keyword')
                ->join('keywords', 'annotation_keyword.keyword_id', '=', 'keywords.id')
                ->join('kvorum_prod.annotations', 'annotation_keyword.annotation_id', '=', 'kvorum_prod.annotations.id')
                ->select('annotation_keyword.thumbnail', 'annotation_keyword.id as ak_id', 'keywords.word', 'keywords.id as k_id', 
                        'kvorum_prod.annotations.id as a_id', 'kvorum_prod.annotations.created_at')
                //provisional filter by channel
                ->where('kvorum_prod.annotations.created_at','>','2019-01-25 00:00:00')->where('keywords.channel',env('APP_CHANNEL', 1))
                ->whereIn('keywords.category_id', [1, 2])
                ->orWhere(function($query) {
                    $query->where('keywords.favorite', 1)
                            //provisional filter by channel
                            ->where('kvorum_prod.annotations.created_at','>','2019-01-25 00:00:00')->where('keywords.channel',env('APP_CHANNEL', 1))
                            ->where('keywords.category_id', 3);
                })
                ->orderBy('annotation_keyword.annotation_id', 'DESC')
                ->take(20)
                ->get();
        //$latestOccurrences = AnnotationKeyword::orderBy('annotation_id', 'DESC')->with('annotation')->with('keyword')->take(20)->get();

        if ($latestOccurrences->count()) {
            $data['latest'] = $this->setOccurrence($latestOccurrences, $requestedVideo, 'latest');
        }
        $data['ios'] = isIOSDevice();
        $data['mobile'] = isMobileDevice();
        return view('home', $data);
    }

    function setOccurrence($occurrences, $requestedVideo, $type) {
        $i = 0;
        $newOccurrences = [];
        foreach ($occurrences as $occurrence) {
            $newOccurrences[$i]['date'] = (new Carbon($occurrence->created_at))->subHours(8)->format('d.m. H:i');
            $newOccurrences[$i]['word'] = $occurrence->word;
            $newOccurrences[$i]['url'] = url('/' . $type . '/' . $occurrence->ak_id);
            $newOccurrences[$i]['playing'] = $requestedVideo == $occurrence->ak_id;
            $newOccurrences[$i]['thumbnail'] = getThumbnail($occurrence->thumbnail);
            Annotation::where('id', '<', $occurrence->a_id + 9)
                    ->where('id', '>', $occurrence->a_id - 9)
                    ->get()->each(function($el)use(&$i, &$newOccurrences) {
                if (isset($newOccurrences[$i]['context']))
                    $newOccurrences[$i]['context'] = $newOccurrences[$i]['context'] . ' ' . $el->word;
                else
                    $newOccurrences[$i]['context'] = $el->word;
            });
            $i++;
        }
        return $newOccurrences;
    }

    function share(Request $request) {
        $data = [];
        $data['time'] = 0;
        $data['word'] = [];
        $data['occurrences'] = [];
        $data['url'] = '';
        $data['current_url'] = url()->full();
        $data['current_thumbnail'] = '';
        $data['sharePage'] = true;
        $data['homePage'] = false;
        $data['title'] = $request->has('title') ? $request->get('title') : 'Kvorum';

        if ($request->has('video')) {
            $requestedVideo = $request->get('video');
            $currentOccurrence = AnnotationKeyword::find($requestedVideo);
            if ($currentOccurrence) {
                $keyword = Keyword::find($currentOccurrence->keyword_id);
                $currentAnnotation = Annotation::find($currentOccurrence->annotation_id);
                $data['url'] = getBucketUrl($currentAnnotation->stream_id);
                $data['current_thumbnail'] = getThumbnail($currentOccurrence->thumbnail);
                $data['time'] = getStartTime($currentAnnotation->start_time);
                $i = 0;
                $occurrences = $keyword->annotation()->take(20)->orderBy('id', 'DESC')->get()->each(function($annotation)use(&$i, $keyword, $requestedVideo) {
                    $this->shareOccurrences[$i]['date'] = $annotation->created_at->subHours(8)->format('d.m. H:i');
                    $occurrence = $annotation->keywords()->where('keywords.id', $keyword->id)->first();
                    $this->shareOccurrences[$i]['url'] = url('/share?video=' . $occurrence->pivot->id);
                    $this->shareOccurrences[$i]['thumbnail'] = getThumbnail($occurrence->pivot->thumbnail);
                    $this->shareOccurrences[$i]['playing'] = $requestedVideo == $occurrence->pivot->id;
                    Annotation::where('id', '<', $annotation->id + 9)
                            ->where('id', '>', $annotation->id - 9)
                            ->get()->each(function($el)use(&$i) {
                        if (isset($this->shareOccurrences[$i]['context']))
                            $this->shareOccurrences[$i]['context'] = $this->shareOccurrences[$i]['context'] . ' ' . $el->word;
                        else
                            $this->shareOccurrences[$i]['context'] = $el->word;
                    });
                    $i++;
                });
                if ($occurrences->count()) {
                    $data['word'] = $keyword->word;
                    $data['occurrences'] = $this->shareOccurrences;
                }
            }
        }

        $data['ios'] = isIOSDevice();
        $data['mobile'] = isMobileDevice();
        return view('share', $data);
    }

    function privacy(Request $request) {
        $data['sharePage'] = false;
        $data['homePage'] = false;
        return view('privacy', $data);
    }

}
