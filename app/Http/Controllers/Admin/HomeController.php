<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\AppUser;
use App\Keyword;
use App\Services\Keywords\KeywordGenerator;
use App\Http\Resources;
use Flugg\Responder\Facades\Responder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        return view('dashboard');
    }
    
    /**
     * quick entry to get the log file only for development
     * @param Request $request
     */

    public function resetOccurences(Request $request) { 
        $lastIdFieldId = 'lastRecordId.txt';

        if ($request->has('put')) {
            Storage::put($lastIdFieldId, $request->get('put'));
        }

        if ($request->has('get')) {
            if (Storage::exists($lastIdFieldId))
                echo Storage::get($lastIdFieldId) ?: 0;
        }

        if ($request->has('get-log')) {
            $fichero = storage_path('logs/laravel.log');
            if (file_exists($fichero)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($fichero) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($fichero));
                readfile($fichero);
                exit;
            }


        }
    }

}
