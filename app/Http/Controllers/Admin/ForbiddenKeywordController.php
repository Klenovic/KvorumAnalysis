<?php

namespace App\Http\Controllers\Admin;

use App\ForbiddenKeyword;
use Illuminate\Http\Request;
use Watson\Validating\ValidationException;
use Flugg\Responder\Facades\Responder;
use App\Http\Controllers\Controller;

class ForbiddenKeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forbidden=ForbiddenKeyword::all();
        return Responder::success($forbidden)->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($keyword=ForbiddenKeyword::make($request->all()))
                return Responder::success(['id'=>$keyword->id]);
            return Responder::error();
        } catch (ValidationException $e) {
            return $this->validationErrorResponse($e);
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ForbiddenKeyword  $forbiddenKeyword
     * @return \Illuminate\Http\Response
     */
    public function show(ForbiddenKeyword $forbiddenKeyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ForbiddenKeyword  $forbiddenKeyword
     * @return \Illuminate\Http\Response
     */
    public function edit(ForbiddenKeyword $forbiddenKeyword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ForbiddenKeyword  $forbiddenKeyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ForbiddenKeyword $forbiddenKeyword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ForbiddenKeyword  $forbiddenKeyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForbiddenKeyword $forbiddenKeyword)
    {
        try {
            $result = $forbiddenKeyword->delete();
            if ($result)
                return Responder::success();
            return Responder::error('ForbiddenKeyword was not deleted! Try again!');
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }
}
