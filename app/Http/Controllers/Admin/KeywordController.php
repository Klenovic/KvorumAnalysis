<?php

namespace App\Http\Controllers\Admin;

use App\Keyword;
use Illuminate\Http\Request;
use App\Events\NewKeywordEvent;
use Watson\Validating\ValidationException;
use Flugg\Responder\Facades\Responder;
use App\Http\Controllers\Controller;
use App\Transformers\KeywordTransformer;

class KeywordController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $keywords = Keyword::with(['category', 'user'])->get();
        return Responder::success($keywords, new KeywordTransformer)->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            if ($keyword=Keyword::make($request->all()))
                return Responder::success(['id'=>$keyword->id,'category'=>$keyword->category->name]);
            return Responder::error();
        } catch (ValidationException $e) {
            return $this->validationErrorResponse($e);
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword) {
        try {
            $result = $keyword->delete();
            if ($result)
                return Responder::success();
            return Responder::error('keyword was not deleted! Try again!');
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    public function favorite(Request $request, Keyword $keyword) {
        try {
            if ($request->get('favorite') == '1') {
                $keyword->favorite = 1;
            } else if ($request->get('favorite') == '0') {
                $keyword->favorite = 0;
            }
            if ($keyword->save())
                return Responder::success();
            return Responder::error();
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

}
