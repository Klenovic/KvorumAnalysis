<?php

namespace App\Http\Controllers\Admin;

use App\Variant;
use App\Keyword;
use App\Annotation;
use Illuminate\Http\Request;
use App\Events\NewVariantEvent;
use Watson\Validating\ValidationException;
use Flugg\Responder\Facades\Responder;
use App\Http\Controllers\Controller;
use App\Transformers\VariantTransformer;

class VariantController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $keywords = Keyword::with(['variant'])->get();
        return Responder::success($keywords, new VariantTransformer)->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            if ($variant = Variant::make($request->all()))
                return Responder::success(['id' => $variant->id]);
        } catch (ValidationException $e) {
            return $this->validationErrorResponse($e);
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function show(Variant $variant) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function edit(Variant $variant) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variant $variant) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variant  $variant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variant $variant) {
        try {
            $keyword = $variant->keyword;
            $result = $variant->delete();
            if ($result) {
                //if no other variant and parent keyword is not in annotations table remove occurrence
                if (!$keyword->variant->count() && !Annotation::where('word', $keyword->word)->get()->count()) {
                    $keyword->annotation()->detach();   
                }

                return Responder::success();
            }

            return Responder::error('Variant was not deleted! Try again!');
        } catch (\Exception $e) {
            return Responder::error($e->getMessage());
        }
    }

}
