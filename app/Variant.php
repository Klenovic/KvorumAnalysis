<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\NewKeywordEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Watson\Validating\ValidatingTrait;
use Watson\Validating\ValidationException;

class Variant extends Model {

    protected $rules = [
        'name' => 'required|string',
        'keyword_id' => 'required|integer',
    ];
    protected $fillable = ['name', 'keyword_id'];

    public function keyword() {
        return $this->belongsTo(Keyword::class);
    }

    public static function make($data) {
        $variant = new self;
        self::validate($data, $variant);
        $variant->fill(array_only($data, $variant->fillable));
        $variant->save();
        event(new NewKeywordEvent($variant));
        return $variant;
    }

    private static function validate($data, $model) {
        $validator = Validator::make($data, $model->rules);
        if ($validator->fails()) {
            throw new ValidationException($validator, $model);
        }
    }

}
