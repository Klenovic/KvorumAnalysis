<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model {

    protected $rules = [
        'name' => 'required|string',
    ];
    protected $fillable = ['name'];

    public function keyword() {
        return $this->hasMany(Keyword::class);
    }

}
