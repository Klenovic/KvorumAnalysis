<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model {
    
    protected $connection= 'annotations_db_connection';

    protected $dates = ['created_at', 'updated_at', 'start_time', 'end_time'];

}
