<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeywordOcurrencde extends Model {

    protected $rules = [
        'annotation_id' => 'required|integer',
        'keyword_id' => 'required|integer',
    ];
    protected $fillable = ['annotation_id', 'keyword_id'];

    //protected $with = [''];

    public function annotation() {
        return $this->belongsTo(Annotation::class);
    }

    public function keyword() {
        return $this->belongsTo(Keyword::class);
    }

   

}
