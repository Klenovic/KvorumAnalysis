<?php

namespace App\Services\FileStorage;

interface CloudStorage
{
    /**
     * Uploads a file to storage and returns the url of the new file.
     *
     * @param $localFilePath string
     * @param $contentType string
     *
     * @return string A URL pointing to the stored file.
     */
    public function storeFile(string $localFilePath, string $contentType): string;

    /**
     * Deletes a file.
     *
     * @param string $url A URL returned by a call to StorageFile.
     */
    public function deleteFile(string $url);

    /**
     * Gets list of objects
     *
     * @param string $prefix  Filter results with this prefix.
     * @param $url
     * @return \Iterator
     */
    public function findObjectsKeys(string $prefix = ''): array;
}