<?php

namespace App\Services\FileStorage;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\StorageObject;

/**
 * class CloudStorage stores images in Google Cloud Storage.
 */
class GoogleCloudStorage implements CloudStorage
{
    private $bucket;

    /**
     * CloudStorage constructor.
     *
     * @param string $bucketName The cloud storage bucket name
     * @param $clientCredentials
     */
    public function __construct($bucketName, $clientCredentials)
    {
        $storage = new StorageClient([
            'keyFile' => $clientCredentials,
            'projectId' => "compengine-207619"
        ]);

        $this->bucket = $storage->bucket($bucketName);
    }

    /**
     * Uploads a file to storage and returns the url of the new file.
     *
     * @param $localFilePath string
     * @param $contentType string
     *
     * @return string A URL pointing to the stored file.
     */
    public function storeFile(string $localFilePath, string $contentType): string
    {
        $f = fopen($localFilePath, 'r');
        $object = $this->bucket->upload($f, [
            'name' => substr($localFilePath, 1),
            'metadata' => ['contentType' => $contentType],
            'predefinedAcl' => 'publicRead',
        ]);

        return $object->info()['mediaLink'];
    }

    /**
     * Deletes a file.
     *
     * @param string $url A URL returned by a call to StorageFile.
     */
    public function deleteFile(string $url)
    {
        $path_components = explode('/', parse_url($url, PHP_URL_PATH));
        $name = $path_components[count($path_components) - 1];
        $object = $this->bucket->object($name);
        $object->delete();
    }

    /**
     * Gets list of objects
     *
     * @param string $prefix  Filter results with this prefix.
     * @return array
     */
    public function findObjectsKeys(string $prefix = ''): array
    {
        $objects = $this->bucket->objects([
            'prefix' => substr($prefix, 1)
        ]);

        $storageSessionsFiles = [];

        /** @var StorageObject $file */
        foreach ($objects as $file) {
            $storageSessionsFiles[] = DIRECTORY_SEPARATOR . $file->name();
        }

        return $storageSessionsFiles;
    }
}
