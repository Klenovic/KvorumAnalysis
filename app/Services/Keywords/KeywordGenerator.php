<?php

namespace App\Services\Keywords;

use App\Keyword;
use App\AppUser;
use App\Annotation;
use Illuminate\Support\Collection;

class KeywordGenerator {

    protected $device;
    protected $appUser;
    protected $occurrence = [];
    protected $occurrenceKey;
    protected $occurrenceChildKey;
    protected $occurrenceDate;

    public function __construct($device = NULL) {
        $this->device = $device;
        $this->appUser = $this->getUserByDevice();
    }

    private function getUserByDevice() {
        return $this->device ? AppUser::whereDeviceId($this->device)->first() : NULL;
    }

    public function userHasKeyword($id): bool {
        return (bool) $this->appUser ? $this->appUser->keywords->where('id', $id)->count() : false;
    }

    /**
     * returns words by category for the user to subscribe
     * @return array
     */
    public function getWordsByCategory() {

        $selectedKeywords = [];
        $themes = Keyword::where('category_id', 1)->whereChannel(env('APP_CHANNEL', 1))->get();
        if ($themes->count()) {
            $themes->map(function($key) use (&$selectedKeywords) {
                $selectedKeywords['themes'][] = ['word' => $key->word, 'id' => $key->id, 'user_has_keyword' => $this->userHasKeyword($key->id)];
            });
        } else {
            $selectedKeywords['themes'] = [];
        }
        $persons = Keyword::where('category_id', 2)->whereChannel(env('APP_CHANNEL', 1))->with(['party', 'province'])->orderby('word', 'ASC')->get();
        if ($persons->count()) {
            $persons->map(function($key) use (&$selectedKeywords) {
                $selectedKeywords['persons'][] = [
                    'word' => $key->word,
                    'id' => $key->id,
                    'province' => isset($key->province->name) ? $key->province->name : '',
                    'user_has_keyword' => $this->userHasKeyword($key->id),
                    'party' => isset($key->party->name) ? $key->party->name : ''
                ];
            });
        } else {
            $selectedKeywords['persons'] = [];
        }

        $subscribedCustomKeywords = !empty($this->appUser) ? $this->appUser->keywords()->where('category_id', 3)->get()->pluck('id') : [];

        $custom = Keyword::where('category_id', 3)->whereChannel(env('APP_CHANNEL', 1))
                        ->where(function($query)use($subscribedCustomKeywords) {
                            $query->where('favorite', 1)
                            ->orWhereIn('id', $subscribedCustomKeywords);
                        })->get();
        if ($custom->count()) {

            $custom->map(function($key) use (&$selectedKeywords) {
                $selectedKeywords['custom'][] = ['word' => $key->word, 'id' => $key->id, 'user_has_keyword' => $this->userHasKeyword($key->id)];
            });
        } else {
            $selectedKeywords['custom'] = [];
        }


        return $selectedKeywords;
    }

    /**
     * return the user's words occurrences 
     * @param type $appUser
     * @return type
     */
    public function getWordsMainList($appUser) {
        $this->occurrence = [];
        $this->occurrenceKey = -1;//var_dump($appUser->keywords()->whereChannel(env('APP_CHANNEL', 1))->get());exit;
        $appUser->keywords()->whereChannel(env('APP_CHANNEL', 1))->get()->each(function($keyword) {
            $this->occurrenceDate = '';
            $this->occurrenceChildKey = 0;
            //$keyword->annotation()->where('annotations.channel',env('APP_CHANNEL', 1))->orderBy('annotations.created_at','DESC')->take(50)->get()->each(function($annotation) use($keyword) {
            $keyword->annotation()->where('annotations.created_at','>','2019-01-25 00:00:00')->orderBy('annotations.created_at','DESC')->take(50)->get()->each(function($annotation) use($keyword) {
                if ($this->occurrenceDate != $annotation->created_at->subHours(8)->format('Y-m-d')) {
                    $this->occurrenceKey++;
                    $this->occurrenceChildKey = 0;
                    $this->occurrenceDate = $annotation->created_at->subHours(8)->format('Y-m-d');
                    $this->occurrence[$this->occurrenceKey]['id'] = $keyword->id;
                    $this->occurrence[$this->occurrenceKey]['word'] = $keyword->word;
                    $this->occurrence[$this->occurrenceKey]['date'] = $this->occurrenceDate;
                    
                }
                
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['time'] = $annotation->created_at->subHours(8)->format('H:i:s');
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['audio_link'] = $annotation->getMedia('a');
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['video_link'] = $annotation->getMedia('v');
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['audio_seek'] = getStartTime($annotation->start_time);
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['video_seek'] = getStartTime($annotation->start_time);
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['stream_id'] = $annotation->stream_id;
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['ocurrence_id'] = $annotation->pivot->id;
               
                
                $thumbnail = $annotation->keywords()->where('keywords.id', $keyword->id)->first()->pivot->thumbnail;
                $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['thumbnail'] = getThumbnail($thumbnail);
                $annotations = Annotation::where('id', '<', $annotation->id + 9)->where('id', '>', $annotation->id - 9)->get();
                $annotations->each(function($el) use($annotation) {
                    if (isset($this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['context']))
                        $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['context'] = $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['context'] . ' ' . $el->word;
                    else
                        $this->occurrence[$this->occurrenceKey]['child'][$this->occurrenceChildKey]['context'] = $el->word;
                });
                $this->occurrenceChildKey++;
            });
            $this->occurrenceKey++;
        });
        return array_values($this->occurrence);
    }

}
