<?php

namespace App\Services\Keywords;

use Illuminate\Support\Collection;
use App\Events\NewKeywordOccureceEvent;
use App\Annotation;

class KeywordOccurences {

    protected $wordLength;

    public function __construct($device = NULL) {
        
    }

    function annotationHasKeywords($annotation, $keywordId) {
        return $annotation->keywords()->where('keywords.id', $keywordId)->get()->count();
    }

    /**
     * Find occurrences for the given word within the given Annotations
     * @param type $annotations
     * @param type $word
     * @param type $keywordId
     */
    public function findOccurences($annotations, $word, $keyWord, $isVariant = false, $isNewWord = false) {
        //$this->$wordLength = str_word_count($word);
        $this->wordLength = getKeywordLength($word);
        $matchedKeywords = [];
        //key to find is one word
        if ($this->wordLength == 1) {
            if (!$isNewWord) {
                $annotations = Annotation::whereWord($word)->whereIn('id', $annotations->pluck('id'))->get();
            }
            foreach ($annotations as $key => $annotation) {
                $this->processOccurrence($annotation, $annotation->word, $keyWord, $word, $isVariant, $isNewWord);
            }
            //key to find is more than one word
        } else {
            
            foreach ($annotations as $key => $annotation) {
                $quertyLength = $this->wordLength - 1;
                $endKey = $key + $quertyLength;
                $wordToSearch = [];
                $id = 0;
                for ($i = $key; $i <= $endKey; $i++) {
                    if (isset($annotations[$i])) {
                        //make sure annotation words are consecutive
                        if ($id == 0 || ($annotations[$i]->id - $id) == 1) {
                            $wordToSearch[] = $annotations[$i]->word;
                        }
                        $id = $annotations[$i]->id;
                    }
                }
                $wordToSearch = implode(' ', $wordToSearch);
                if (strtolower($wordToSearch) != strtolower($word))
                    continue;
                //var_dump('$wordToSearch: ' . $wordToSearch . '$word: ' . $word);
                //\Log::debug('$wordToSearch: ' . $wordToSearch . '$word: ' . $word. '$quertyLength: ' . $quertyLength);
                $this->processOccurrence($annotation, $wordToSearch, $keyWord, $word, $isVariant, $isNewWord);
            }
        }
    }

    function processOccurrence($annotation, $wordToSearch, $keyWord, $word, $isVariant, $isNewWord) {
        $annotationHasKeywords = $this->annotationHasKeywords($annotation, $keyWord->id);
        if (strtolower($wordToSearch) == strtolower($word) && !$this->skipForRepetition($annotation, $keyWord) && (!$annotationHasKeywords || $isVariant)) {
            //only attach if annotation not previously attached with keyword or it is a variant
            if (!$isVariant || !$annotationHasKeywords) {
                $annotation->keywords()->attach($keyWord->id);
                $annotationKeyword = $annotation->keywords()->where('keywords.id', $keyWord->id)->orderBy('id', 'DESC')->first();
                $annotationKeyword->setThumbnail($annotation);

                \Log::debug('********************New Occurrence for word: ' . $word . '. id:' . $keyWord->id . '. annotationid: ' . $annotation->id);
                //do not send notifications when new keyword
                if (!$isNewWord) {
                    //$annotationKeyword = $annotation->keywords()->find($keyWord->id);
                    if ($annotationKeyword) {
                        $annotationKeyword->realWord = $word;
                        $annotationKeyword->annotationModel = $annotation;
                    }
                    event(new NewKeywordOccureceEvent($annotationKeyword));
                }
            }
        }
    }

    /**
     * Skip word if it has are been mentioned within the last 5 minutes in the same stream
     * @param type $annotation
     * @return bool
     */
    private function skipForRepetition($annotation, $keyWord): bool {
        //if complex word do not search for skipping to not comprmised performance, also there is a low chance of repetition
        if ($this->wordLength > 1) {

            $skipForRepetition = \DB::table('annotation_keyword')
                    ->join('kvorum_prod.annotations', 'annotation_keyword.annotation_id', '=', 'kvorum_prod.annotations.id')
                    ->select('annotation_keyword.annotation_id')
                    ->where('kvorum_prod.annotations.id', '>', $annotation->id)
                    ->where('kvorum_prod.annotations.start_time', '<', ($annotation->start_time + 300))
                    ->where('annotation_keyword.keyword_id', $keyWord->id)
                    ->first();
        } else {

            //group where in skipe repetition to skip current variant. its siblings, its parent, current keyword and its variants
            $skipForRepetition = Annotation::whereStreamId($annotation->stream_id)
                            ->where('start_time', '>', $annotation->start_time - 300)
                            ->where('start_time', '<', $annotation->start_time)
                            ->where(function ($where) use($annotation, $keyWord) {
                                $where->whereWord($annotation->word)->orWhere('word', $keyWord->word)->orWhereIn('word', $keyWord->variant->pluck('name'));
                            })->count();
        }








        \Log::debug('skipForRepetition: word:' . $annotation->word . ',  result:' . $skipForRepetition . ', annotId:' . $annotation->id . ', $keyWord:' . $keyWord->word);
        return (bool) $skipForRepetition;
    }

}
