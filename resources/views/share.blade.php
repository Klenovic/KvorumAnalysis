@extends('layouts.public-app')
@section('content')
<div class="container">
    <section class="home-page">
        <h2 class="keyword-title"><b>Kvorum video streaming</b></h2>

        @if($url)
        @if($ios)
        <video id="videoNative" controls autoplay autobuffer webkit-playsinline="true" playsinline="true">
            <source src="{{$url}}#t={{$time}}">
        </video>
        <script>
            document.getElementById('videoNative').addEventListener('loadedmetadata', function() {
            console.log(this);
            this.currentTime = {{$time}};
            }, false);
        </script>
        @else
        <video id="video" controls autoplay></video>
        <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
        <script>
            var video = document.getElementById('video');
            if (Hls.isSupported()) {
            var config = {
            startPosition: {{$time}}
            }
            var hls = new Hls(config);
            hls.loadSource('{{$url}}');
            hls.attachMedia(video);
            hls.on(Hls.Events.MANIFEST_PARSED, function () {
            });
            }
        </script>
        @endif
        @endif
    </section>
    <section class="related-videos">

        <div class="container">
            <div class="row">
                
                <h2 class="share-description">
                    <b>Budi u toku da saznaš u roku.</b><br>
                    Koristi aplikaciju koja ti javlja u stvarnom vremenu kada u Saboru govore i odlučuju o temama koje si ti odabrao/la, to jest koje su tebi važne. Neka vijesti prate tebe.
                </h2>
                <h3>Slični segmenti vezani za temu ili osobu:</h3>
                @if (count($occurrences))
                @foreach ($occurrences as $key=>$occurrence)
                <div class="col-md-12 related-item item-latest-count-{{$key}}" data-time="10" data-url="{{$occurrence['url']}}">
                    <div class="row related-videos-row">
                        <div class="col-sm-2 text-center" >
                            <div class="thumbnail-content">
                                @if ($occurrence['playing'])  <img class="video-playing" src="imgs/play.png"> @endif
                                <img class="video-thumb" src="{{$occurrence['thumbnail']}}">
                            </div>
                        </div>
                        <div class="col-sm-10 related-context">

                            ...{{$occurrence['context']}}...
                            <p class="video-date">{{$occurrence['date']}}h</p>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="text-center" style="margin: 20px auto; font-size: 20px;">No Occurrences</p>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn btn-default page-more-latest" onclick="nextPageLatest();" >Još ...</button>
                </div>
            </div>
        </div>
    </section>

</div>
<link href="{{ asset('css/style.css') }}" rel="stylesheet">


<script>
    var perPage = 5;
    var current = 0;
    $(function() {
    $('.related-item').click(function() {
    window.location = $(this).data('url');
    });
    $("#tabs").tabs();
    });
    nextPage();
    function nextPage() {
    var next = current + perPage - 1;
    for (var p = current; p <= next; p++) {
    $('.item-count-' + p).show();
    current++;
    }
    if (current >= 20)
            $('.page-more').hide();
    }
    var currentLatest = 0;
    nextPageLatest()
            function nextPageLatest() {
            var nextLatest = currentLatest + perPage - 1;
            for (var p = currentLatest; p <= nextLatest; p++) {
            $('.item-latest-count-' + p).show();
            currentLatest++;
            }
            if (currentLatest >= 20)
                    $('.page-more-latest').hide();
            }
</script>
@endsection
