<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @if($sharePage || $homePage)

        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="{{$title}}" />
        @if($sharePage)
        <meta property="og:description" content="{{$word}}" />
        @elseif($homePage)
        <meta property="og:description" content="Budi u toku da saznaš u roku" />
        @endif
        
        
        
        
        <meta property="og:url" content="{{$current_url}}" />
        <meta property="og:site_name" content="Kvorum" />
        <meta property="og:image" content="{{$current_thumbnail}}" />
        <meta property="og:image:secure_url" content="{{$current_thumbnail}}" />
        <meta property="og:image:width" content="256" />
        <meta property="og:image:height" content="200" />
        @endif

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{$title}}</title>

        <link rel="stylesheet" href="//releases.flowplayer.org/7.0.4/commercial/skin/skin.css">

        <link rel="shortcut icon" type="image/png" href="/imgs/favicon.png" id="favicon" />

        <!-- Scripts 
        <script src="{{ asset('js/app.js') }}" defer></script>
        -->


        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">



        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!--        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <img class="kvorum-logo" src="/imgs/logo.jpg">



                </div>
            </nav>

            <main class="py-4">
                @yield('content')
                @if($sharePage || $homePage)
                <footer style="padding: 20px; text-align: center;">


                    <h3>Preuzmi mobilnu aplikaciju još danas:</h3>
                    @if($sharePage || $homePage)
                    <ul class="app-links">
                        @if($mobile)
                        @if($ios)
                        <li>
                            <a class="navbar-brand store-nav" href="itms://itunes.apple.com/us/app/apple-store/id1424967989?mt=8">
                                <img src="/imgs/app-store.jpg">
                            </a>
                        </li>
                        @else
                        <li>
                            <a class="navbar-brand google-nav" href="https://play.google.com/store/apps/details?id=hr.kvorum.android">
                                <img src="/imgs/google-play.jpg">
                            </a>
                        </li>
                        @endif
                        @else
                        <li>
                            <a class="navbar-brand google-nav" href="https://play.google.com/store/apps/details?id=hr.kvorum.android">
                                <img src="/imgs/google-play.jpg">
                            </a>
                        </li>
                        <li>
                            <a class="navbar-brand store-nav" href="itms://itunes.apple.com/us/app/apple-store/id1424967989?mt=8">
                                <img src="/imgs/app-store.jpg">
                            </a>
                        </li>
                        @endif
                    </ul>
                    @endif

                    <ul class="footer-bottom">
                        <li><a href="/privacy">Privatnost</a></li>
                        <li><a href="mailto:kvorumhr@gmail.com">kvorumhr@gmail.com</a></li>
                    </ul>



                </footer>
                @endif

            </main>

        </div>

    </body>
</html>
