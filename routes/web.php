<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Annotation;
use App\Keyword;
use App\Services\Keywords\KeywordOccurences;
//use App\Services\FileStorage\GoogleCloudStorage;
use App\Events\NewKeywordOccureceEvent;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

Route::get('/testNotification', function () {
    
    $ak= \App\AnnotationKeyword::find(rand(500,850));
    if (!$ak) {
        echo 'Try Again';
        exit;
    }
    $annotationKeyword=$ak->annotation->keywords()->where('keywords.id', $ak->keyword_id)->orderBy('id', 'DESC')->first();
    
    
    
    
    
    
    $annotationKeyword->realWord = '$word';
    $annotationKeyword->annotationModel = $ak->annotation;
    $annotationKeyword->testing = true;
    

    event(new NewKeywordOccureceEvent($annotationKeyword));
    echo 'Sent<br>';
    echo 'Word: ' . $annotationKeyword->word;
    //echo '<br>Annotation: ' . $annotationKeyword->annotation->word;
    exit;




    $optionBuilder = new OptionsBuilder();
    $optionBuilder->setTimeToLive(60 * 20);

    $notificationBuilder = new PayloadNotificationBuilder('my title');
    $notificationBuilder->setBody('Hello world')
            ->setSound('default');

    $dataBuilder = new PayloadDataBuilder();
    $dataBuilder->addData(['a_data' => 'my_data']);

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $data = $dataBuilder->build();



    //$token = 'cxz4aMEY5gc:APA91bF1Q0lBtRfmKWgQRGbW-Y6TnN7tNHBXBr7pVDMZSUGIt2Bw1egCbopiI0gmPrKDmW7PU1wnD4yoxyhwb-vME2Zg2pccTfhXgsz8KOSdEX--CialCVRgpQtkFj8-236w9vLBNSZjfNZ7-9FTo-sj3174mlLsnA';
    $token = [
        'fc66hnPP4nc:APA91bHQ0gW6MkNJsGnk8WIHu0m7lXp99GDBCy86C2ExmnVDVNdV3HjK2JonAr6_RCRGWlATcO3_FEJJIDtCt0m-P8LbTchN4GapvmJ5tIj1yz33VtqJKd3vHZ9DTGPUh2dtiOuIxVLP',
        'ddH3_8Qknn4:APA91bFystUJQllP3_2452MXbePS7gvgSELbQUiNRqmwsztSOhPF3Ig8op_v-QS_cI7tVfip1eZse0ey6afriULlq-VkB37MeKZ988DeILbU_kGMHZ5Owpr1Qw68nQNmOmOiFf996l-H'
    ];

    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

    print_r($downstreamResponse);
    exit;



    $occurrences = \App\AnnotationKeyword::orderBy('id', 'ASC')->get();
    $occurrences->each(function($occurrence) {
        if (!file_exists(storage_path('app/public/' . $occurrence->thumbnail))) {
            //var_dump('no exists' . storage_path('app/public/' . $occurrence->thumbnail));
            $occurrence->thumbnail = null;
            $occurrence->save();
        }
    });

    exit;

    $array = get_m3u8_video_segment('https://storage.googleapis.com/kvorum_test_bucket/app/video/6139/v/out.m3u8', '1526.600');
    var_dump($array);
    exit;


    $disk = Storage::disk('gcs');

    $path = '/home/dev/Pictures/Selection_051.png';
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

// create a file
    var_dump($disk->put('app/ffmpeg/11/Selection_054.ts.png', $path));

    exit;


    $storageClient = new StorageClient([
        'projectId' => 'compengine-207619',
    ]);
    $bucket = $storageClient->bucket('fran-bucket');
    $adapter = new GoogleStorageAdapter($storageClient, $bucket);
    $filesystem = new Filesystem($adapter);


    $path = '/home/dev/Pictures/Selection_051.png';
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);


    var_dump($filesystem->write('app/ffmpeg/11/Selection_053.ts.png', $base64));

    exit;








    $adapterOptions = [
        'projectId' => 'compengine-207619',
        'bucket' => 'fran-bucket',
        'prefix' => '',
    ];
    $adapter = new GoogleCloudStorageAdapter(null, $adapterOptions);

    $filesystem = new Filesystem($adapter);

    var_dump($filesystem);


    //Storage::disk('google')->put('file.txt', 'Contents');
    exit;



//    $googleClientCredentials = 'C:\Users\francisco\Downloads\compEngine-acebf6e7f096.json';
//    $bucketName = getenv('GOOGLE_STORAGE_BUCKET');
//    $store = new GoogleCloudStorage($bucketName, $googleClientCredentials);
//
//    var_dump($store->storeFile('D:\pictures\logobusinesreg.jpg', mime_content_type('D:\pictures\logobusinesreg.jpg')));
//
//    exit;




    exit;








    $optionBuilder = new OptionsBuilder();
    $optionBuilder->setTimeToLive(60 * 20);

    $notificationBuilder = new PayloadNotificationBuilder('New data related to your subscriptions');
    $notificationBuilder->setBody('The word: testWord Has been referenced')
            ->setSound('default');

    $dataBuilder = new PayloadDataBuilder();
    $dataBuilder->addData(['word' => '$event->keyword->word']);

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $data = $dataBuilder->build();

    $tokens = App\AppUser::where('device_token', '!=', '')->where('device_token', '!=', 'null')->whereNotNull('device_token')->pluck('device_token')->toArray();
    var_dump(implode(',', $tokens));

    $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    print_r($downstreamResponse);



    var_dump('numberSuccess: ' . $downstreamResponse->numberSuccess());
    var_dump('numberFailure: ' . $downstreamResponse->numberFailure());
    var_dump('numberModification: ' . $downstreamResponse->numberModification());

    exit;




















    $word = 'pravosuđe';



    $newAnnotations = Annotation::where('word', $word)->get();

    if ($newAnnotations->count()) {
        var_dump('new cron process starts ' . print_r($newAnnotations->toArray(), true));

        $keywords = Keyword::where('word', $word)->each(function($keyword)use($newAnnotations) {
            $word = $keyword->word;
            $id = $keyword->id;
            (new KeywordOccurences())->findOccurences($newAnnotations, $word, $id);
        });
    } else {
        echo 'No new records';
    }
    exit;
});


//provisional way of serveing root domain
Route::get('/', 'HomeController@provisionalHome');
Route::get('/share', 'HomeController@share');
Route::get('/privacy', 'HomeController@privacy');
Route::get('/get-logs', 'Admin\HomeController@resetOccurences');
Route::get('{type}/{annotationKeyword?}', 'HomeController@index')->where('type', '(latest)|(popular)');


Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');
    //Route::post('/keywords', 'KeywordController@store');
});

