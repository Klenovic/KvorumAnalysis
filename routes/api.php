<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//admin in web route
Route::group(['prefix' => 'keywords'], function () {
    Route::get('/', 'Admin\KeywordController@index');
    Route::post('/', 'Admin\KeywordController@store');
    Route::put('{keyword}', 'Admin\KeywordController@favorite');
    Route::delete('{keyword}', 'Admin\KeywordController@destroy');
});
Route::group(['prefix' => 'variants'], function () {
    Route::get('/', 'Admin\VariantController@index');
    Route::post('/', 'Admin\VariantController@store');
    Route::delete('{variant}', 'Admin\VariantController@destroy');
});
Route::group(['prefix' => 'forbidden-keywords'], function () {
    Route::get('/', 'Admin\ForbiddenKeywordController@index');
    Route::post('/', 'Admin\ForbiddenKeywordController@store');
    Route::delete('{forbiddenKeyword}', 'Admin\ForbiddenKeywordController@destroy');
});
Route::get('/categories', 'Admin\CategoryController@index');
Route::get('/parties', 'Admin\PartyController@index');
Route::get('/provinces', 'Admin\ProvinceController@index');
Route::get('/reset-occurrences', 'Admin\HomeController@resetOccurences');




//Route::resource('annotation','AnnotationController');

Route::get('/video-view/{keywordOcurrence}', 'AnnotationKeywordController@counter');
//Route::group(['middleware' => 'hasDevice'], function () {
    Route::get('/user-keyword', 'AppUserController@index');
    Route::put('/user-keyword', 'AppUserController@update');
    Route::group(['middleware' => 'registered'], function () {

        Route::get('/user-occurences', 'KeywordOcurrenceController@index');
    });
//});





