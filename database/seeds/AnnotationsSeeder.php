<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AnnotationsSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        /*
          DB::statement('SET FOREIGN_KEY_CHECKS=0;');





          try {
          DB::table('streams')->truncate();
          DB::statement("INSERT INTO `streams` (`id`, `container_id`, `start_time`, `end_time`, `created_at`, `updated_at`) VALUES
          (2, '5d9346cec46baa63c6e99bc656136e6fd3537aab9e79060951bf282c6fbd2b60', '2018-05-16 16:55:15', '2018-05-16 16:57:23', '2018-05-16 16:55:15', '2018-05-16 16:57:23'),
          (3, '4047b4b15eb73305b3a4e87e1aa1d6115e925c040442e570363d5ae9140c5057', '2018-05-16 18:18:06', '2018-05-16 18:44:48', '2018-05-16 18:18:05', '2018-05-16 18:44:48'),
          (4, '81eb1a1427dc927112c1314c283112723c14a88d6b959b6d625928d48541951c', '2018-05-17 14:28:58', '2018-05-17 14:32:19', '2018-05-17 14:28:58', '2018-05-17 14:32:19'),
          (5, '4dbc0da190a518274049f90b2971e349896943201834a23417ee6f4390795d21', '2018-05-17 15:33:52', '2018-05-17 15:35:12', '2018-05-17 15:33:51', '2018-05-17 15:35:12'),
          (6, '8a40b8bbad108746c27fec817b07a15811898c8bb84bfa5fea3c24e9448d9406', '2018-05-17 16:56:38', '2018-05-17 17:02:12', '2018-05-17 16:56:37', '2018-05-17 17:02:12'),
          (7, '4f1922e3577bfd32804378b265aef51650ab6ce544376b07c6733453c80da216', '2018-05-17 17:18:21', '2018-05-17 17:22:13', '2018-05-17 17:18:21', '2018-05-17 17:22:13'),
          (8, '22f815d623d980d7bbca2fa69ad2228bed4d34251b11b52583681e0c04d08456', '2018-05-18 13:22:26', '2018-05-18 13:28:28', '2018-05-18 13:22:25', '2018-05-18 13:28:28'),
          (9, 'fc896b5e4503d58a4ab5b77feb895fd2e096c2849bdbfd5f6ae8843a8c2ebf0f', '2018-05-18 13:35:47', '2018-05-18 14:25:18', '2018-05-18 13:35:46', '2018-05-18 14:25:18');");
          } catch (Exception $e) {
          dd($e->getMessage());
          }
         * 
         */




        try {
            $faker = Faker::create();
            $text = $faker->realText(500, 2);
            $cleanText = preg_replace('/[^a-zA-Z0-9 ]/', '', $text);
            

            DB::statement("INSERT INTO `annotations` (`id`, `stream_id`, `start_time`, `end_time`, `word`, `letters_rate`, `created_at`, `updated_at`) VALUES
(3028, 9, '2370.000', '2370.600', 'it', '0.3000', '2018-05-18 14:25:23', '2018-05-18 14:25:23');");
        } catch (Exception $e) {
            dd($e->getMessage());
        }

        //DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
