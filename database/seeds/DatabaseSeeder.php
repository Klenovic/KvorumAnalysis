<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\AppUser;
use App\Party;
use App\Province;
use App\Category;
use App\Keyword;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('app_users')->truncate();
        for ($i = 1; $i <= 10; $i++) {
            $data = ['device_id' => $i];
            $user = AppUser::create($data)->save();
        }

        
        
        DB::table('categories')->truncate();
        $categories = [
            [
                'name' => 'theme',
               
            ],
            [
                'name' => 'person',
               
            ],
            [
                'name' => 'custom',
               
            ],
            
        ];

        try {
            foreach ($categories as $category) {
                Category::create($category)->save();
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
        

        DB::table('parties')->truncate();
        for ($i = 1; $i <= 10; $i++) {
            $data = ['name' => 'party' . $i];
            $user = Party::create($data)->save();
        }

        DB::table('provinces')->truncate();
        for ($i = 1; $i <= 10; $i++) {
            $data = ['name' => 'province' . $i];
            $user = Province::create($data)->save();
        }

        DB::table('keywords')->truncate();
        $keywords = [
            [
                'word' => 'Ole',//1845
                'category_id' => 2,
                'province_id' => 1,
                'party_id' => 1,
            ],
            [
                'word' => 'kamerom',//1364
                'category_id' => 2,
                'province_id' => 2,
                'party_id' => 1,
            ],
            [
                'word' => 'United Kingdom',//2118
                'category_id' => 3,
               
            ],
            [
                'word' => 'golf',//2986
                'category_id' => 3,
            ],
            [
                'word' => 'Europe',//2697
                'category_id' => 3,
            ],
        ];

        try {
            foreach ($keywords as $keyword) {
                Keyword::create($keyword)->save();
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
