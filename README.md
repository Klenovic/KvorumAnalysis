
# KvorumAnalysis Development Environment

## Prerequisites

- [node.js & npm](https://nodejs.org/)
- [laravel](http://laravel.com/)
- [composer](https://getcomposer.org/)
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension

## Before installation (extract /app/storage from old container)

docker exec -ti kvorumanalysis_kvorumanalysis_1 bash


In container, in /app directory:

tar cfvz storage.tar.gz storage/


Outside container:

docker cp kvorumanalysis_kvorumanalysis_1:/app/storage.tar.gz /tmp/

## Installation

from the root folder:

docker login registry.gitlab.com

docker build -t registry.gitlab.com/klenovic/kvorumanalysis .

docker push registry.gitlab.com/klenovic/kvorumanalysis

docker-compose -f docker-compose.builders.yml up --build

docker-compose up -d

docker-compose exec kvorumanalysis composer install

docker-compose exec kvorumanalysis php artisan key:generate

docker-compose exec kvorumanalysis php artisan migrate

docker cp /tmp/storage.tar.gz kvorumanalysis_kvorumanalysis_1:/tmp/

docker exec -ti kvorumanalysis_kvorumanalysis_1 bash


In cointainer, in /app directory:

tar xfvz /tmp/storage.tar.gz

curl -sL https://deb.nodesource.com/setup_6.x | bash

apt-get install -y nodejs


Outside container:

docker-compose exec kvorumanalysis npm install

docker-compose exec kvorumanalysis npm run production

docker-compose exec kvorumanalysis chown application:application /app/storage/logs/laravel.log

docker-compose exec kvorumanalysis chmod g+w /app/storage/logs/laravel.log

docker-compose exec kvorumanalysis php artisan storage:link

